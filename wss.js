const log = require("./log");
const WebSocketServer = require("ws").Server;

const connections = [];

let wss;

module.exports.tellAll = message => {
  connections.forEach(ws => {
    try {
      ws.send(message);
    } catch (e) {
      log("error", "Issue sending WS message " + message + "\n\n" + e);
    }
  });
};

module.exports.create = server => {
  wss = new WebSocketServer({ server: server });
  log("info", "websocket server created");

  wss.on("connection", ws => {
    connections.push(ws);

    ws.on("close", () => {
      log("info", "websocket connection close");
      connections.splice(connections.indexOf(ws), 1);
    });

    log("info", "websocket connection open");
  });
};
