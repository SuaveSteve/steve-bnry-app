# steve-bnry-app

## Structure

The project is essentially two parts,
the React frontend (`./react-ui`) and the Express (`./`) backend.

## Local Running

The app will try to connect to a local mongodb.

```bash
$ npm i
$ npm run heroku-postbuild #regardless whether on Heroku
$ npm start
```

You will see output on where server is running.

You can change the location of the DB easily by running like so:

```bash
$ MONGODB_URI=<connection-string> npm start
```

## Testing out the live changing

There are no forms, but some simple commands will cause things to update.

### Adding a new file

We can `POST` a new image like so:

```bash
curl -F 'image=@<path-to-file>' -F 'description=<your-description>' http://localhost:5000/api/image
```

#### Example

There's already a `cat.jpg` the repo you can upload to the server.

```bash
curl -F 'image=@cat.jpg' -F 'description=This is my favourite cat!' http://localhost:5000/api/image
```

If you have the browser open, the carousel should have slid to the new image along with a notification.

### Changing a description

We can `PATCH` an image like so:

```bash
curl -X PATCH -H "Content-Type: application/json" -d '{"description": <new-description>}' http://localhost:5000/api/image/<id>
```

#### Example

```bash
curl -X PATCH -H "Content-Type: application/json" -d '{"description": "This may not be my favourite cat."}' http://localhost:5000/api/image/cat.jpg
```

If you have the browser open with the same image in view, the description should change immediately, along with a notification.

## Deleting app's data from local machine

```bash
mongo steve-bnry-app --eval "db.dropDatabase()"
```

## Deployment

The app is ready to be deployed to Heroku.
Assuming the remote `heroku` is in git:

```bash
git push heroku <source-branch>:master --force
```
