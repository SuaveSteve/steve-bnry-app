const express = require("express");
const http = require("http");
const path = require("path");
const db = require("./db");

const wss = require("./wss");
const log = require("./log");
const api = require("./api");

const PORT = process.env.PORT || 5000;

const app = express();

app.use(express.static(path.resolve(__dirname, "./react-ui/build")));
app.use(express.static(path.resolve(__dirname)));
app.use(express.json());

app.use("/api", api);

app.get("/", (request, response) => {
  response.sendFile(path.resolve(__dirname, "../react-ui/build", "index.html"));
});

app.use((req, res, next) => {
  res.status(404).send("\nNOT FOUND\n");
});

app.use((err, req, res, next) => {
  log("error", err);
  res.status(500).send("\nCRITICAL ERROR\n");
});

const startServer = () => {
  const server = http.createServer(app);

  wss.create(server);

  server.listen(PORT, () => {
    log("info", `Express app listening on port ${PORT}`);
  });
};

db.connect(startServer);
