import React, { Component } from "react";
import Slider from "react-slick";

import "./App.css";

const Notyf = require("notyf");

const notyf = new Notyf({ delay: 6000 });

class App extends Component {
  constructor(props) {
    super(props);

    this.state = {
      images: [],
      currentImage: null,
      currentIndex: 0
    };
  }

  async componentDidMount() {
    const response = await fetch("/api/image");
    const json = await response.json();

    this.setState({
      images: json
    });

    const url =
      process.env.REACT_APP_WS_SERVER || "ws://" + window.location.host;

    const ws = new WebSocket(url);

    ws.onopen = function open() {
      notyf.confirm("WebSocket connection made!");
    };

    ws.onmessage = wsMsg => {
      const message = JSON.parse(wsMsg.data);

      switch (message.type) {
        case "update": {
          const index = this.state.images.findIndex(
            image => image._id === message._id
          );

          if (index !== -1) {
            const images = [...this.state.images];

            const image = { ...images[index], ...message.update };

            images[index] = image;

            this.setState({ images: images });

            notyf.confirm(`Server provided an update for ${message._id} !`);
          }

          console.log(index);

          break;
        }

        case "new": {
          const images = [...this.state.images, message.image];

          this.setState({ images: images }, () => {
            this.refs.slider.slickGoTo(images.length - 1);
            notyf.confirm(`Server provided a new image ${message.image._id} !`);
          });

          break;
        }

        default: {
          notyf.alert(`Server gave message with unknown type. See log.`);
          console.error("unknown message type\n\n" + message);
        }
      }
    };

    ws.onmessage.bind(this);
  }

  handleNextClick = e => {
    this.refs.slider.slickNext();
  };

  handlePrevClick = e => {
    this.refs.slider.slickPrev();
  };

  render() {
    const slickSettings = {
      dots: false,
      infinite: true,
      speed: 500,
      slidesToShow: 1,
      slidesToScroll: 1,
      arrows: false,
      centerPadding: "0px",
      lazyLoad: "progressive",
      beforeChange: (oldIndex, newIndex) => {
        this.setState({ currentIndex: newIndex });
      }
    };

    return (
      <div className="App">
        <Slider className={"slider"} ref={"slider"} {...slickSettings}>
          {this.state.images.map(image => (
            <div key={image._id} className={"img-container"}>
              <span className={"left-side"} onClick={this.handlePrevClick} />
              <span className={"right-side"} onClick={this.handleNextClick} />

              <img src={image.links.bin} />
            </div>
          ))}
        </Slider>

        {this.state.images.length && (
          <div className={"info-box"}>
            <dl>
              <dt>ID</dt>
              <dd>{this.state.images[this.state.currentIndex]._id}</dd>

              <dt>Description</dt>
              <dd>{this.state.images[this.state.currentIndex].description}</dd>
            </dl>
          </div>
        )}
      </div>
    );
  }
}

export default App;
