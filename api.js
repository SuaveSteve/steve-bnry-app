const router = require("express").Router();
const multer = require("multer");
const storage = multer.memoryStorage();
const upload = multer({
  storage: storage,
  limits: {fileSize: 50000000 /* 50 MB */}
});

const db = require("./db");
const wss = require("./wss");
const log = require("./log");

const asyncWrap = fn => (...args) => fn(...args).catch(args[2]);

router
  .route("/image/:id")
  // update an image
  .patch(
    asyncWrap(async (req, res, next) => {
      const filter = {
        _id: req.params.id
      };

      const update = {
        $set: {description: req.body.description}
      };

      const results = await db.client
        .collection("images")
        .updateOne(filter, update);

      if (results.matchedCount === 0) {
        next();
        return;
      }

      log("info", `Update ${filter._id} in DB`);

      const message = {
        type: "update",
        _id: filter._id,
        update: update.$set
      };

      wss.tellAll(JSON.stringify(message));

      res.status(200).send();
    })
  );

router
  .route("/image")
  // Respond with a list of all images
  .get(
    asyncWrap(async (req, res, next) => {
      const filter = {};
      const options = {projection: {buffer: false}, sort: {_id: 1}};

      const results = await db.client
        .collection("images")
        .find(filter, options)

        .toArray();

      results.forEach(result => {
        result.links = {bin: `/api/image/${result._id}/bin`};
      });

      res.set("Content-Type", "application/json");
      res.send(results);
    })
  )
  // Accept a new image
  .post(
    upload.single("image"),
    asyncWrap(async (req, res, next) => {
      const toInsert = {
        _id: req.file.originalname,
        mimetype: req.file.mimetype,
        buffer: req.file.buffer,
        description: req.body.description
      };

      try {
        await db.client
          .collection("images")
          .insertOne(toInsert);
      } catch (e) {

        if (e.code === 11000) {
          res.status(409).send('\nCONFLICT\n');
          return;
        }

        throw e;

      }

      log("info", `Wrote ${toInsert._id} to DB`);

      const message = {
        type: "new",
        image: {
          _id: req.file.originalname,
          description: req.body.description,
          links: {bin: `/api/image/${toInsert._id}/bin`}
        }
      };

      wss.tellAll(JSON.stringify(message));

      res.status(200).send();
    })
  );

// Respond with an image's binary
router.route("/image/:id/bin").get(
  asyncWrap(async (req, res, next) => {
    const result = await db.client
      .collection("images")
      .findOne({_id: req.params.id});

    if (result === null) {
      next();
      return;
    }

    const imgBlob = result.buffer.buffer;
    const contentType = result.mimetype;
    const filename = encodeURIComponent(result._id);

    res.set("Content-disposition", 'inline; filename="' + filename + '"');
    res.set("Content-Type", contentType);

    res.send(imgBlob);
  })
);

module.exports = router;
