module.exports = (level, msg) => {
  console.log(`[${new Date().toISOString()}] [${level}] ${msg}`);
};
