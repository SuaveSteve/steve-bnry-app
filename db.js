const path = require("path");
const fs = require("fs-extra");
loremIpsum = require("lorem-ipsum");
const fileType = require("file-type");

const log = require("./log");

const MongoClient = require("mongodb").MongoClient;
const assert = require("assert");

// Connection URL
const url = process.env.MONGODB_URI || "mongodb://localhost:27017";

// Database Name
const dbName = "steve-bnry-app";

// Create a new MongoClient
const client = new MongoClient(url, { useNewUrlParser: true });

let db;

module.exports.connect = async cb => {
  await client.connect();

  log("info", "Connected successfully to server");

  module.exports.client = db = client.db(dbName);
  // module.exports.close = client.close;

  await loadImages();

  cb();
};

const loadImages = async () => {
  const starterImgsPath = "./starterImages";

  const files = await fs.readdir("./starterImages");

  for (let file of files) {
    const filePath = path.join(starterImgsPath, file);

    const data = await fs.readFile(filePath);

    const toInsert = {
      _id: file,
      mimetype: fileType(data).mime,
      buffer: data,
      description: loremIpsum({ count: 10, units: "words" })
    };

    await db
      .collection("images")
      .replaceOne({ _id: file }, toInsert, { upsert: true });

    log("info", `Wrote ${filePath} to DB`);
  }
};
